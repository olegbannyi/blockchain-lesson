# Blockchain with JavaScript

Example of blockchain project

## Description
This project will get you through the source code of the blockchain app developed based on JavaScript programming language.
For detailed tutorial see [Learn Blockchain Programming with JavaScript](https://www.packtpub.com/web-development/learn-blockchain-programming-javascript)
