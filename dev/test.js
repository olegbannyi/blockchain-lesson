const Blockchain = require("./blockchain");

const bitcoin = new Blockchain();

bitcoin.createNewBlock(2398, "LSKDHLKSDJSGHJ", "s98d6c9ds879s");
bitcoin.createNewTransaction(100, "ALEXLSKDHLKSDJSGHJ", "JENNs98d6c9ds879s");
bitcoin.createNewBlock(548764, "8S7D6F78S6F86S", "876SDC78YSHJKDHSDKH");

const previousBlockHash = "DIVDF98DF0D89FV79SDYSJ";
const currentBlockData = [
    {
        amount: 10,
        sender: "9D8F7V09DF80DS908CS",
        recipient: "0D9F8V0D9S8SDSDKJ0"
    }
];

const nonce = bitcoin.proofOfWork(previousBlockHash, currentBlockData);

const hash = bitcoin.hashBlock(previousBlockHash, currentBlockData, nonce);

console.log(bitcoin);
console.log(hash);
console.log(nonce);
